/**
 * Created by The Exiled on 1/8/2559.
 */
var app = new Vue({
    el  : '#app',
    components : {
        'alert' : alert
    },
    ready : function () {
        var self = this;
        document.title = 'ระบบโหวตดาวเดือนคณะบริหารธุระกิจ';
        self.authenticate()
            .then(function (response) {
               if(response) {
                   self.$set('authentication',response.authentication);
                   self.$set('isAdministrator',response.isAdministrator);
               }
            },function (err) {
                console.log(err)
            });
    },
    data : {
        email : '',
        password : '',
        authentication : false,
        isAdministrator: false,
        alertMessage : 'ไม่มีการทำรายการ',
        alertSuccessMsg : 'ไม่มีการทำรายการ',
        alertSuccessShow : false,
        alertShow : false
    },
    methods : {
        onLogin : function (e) {
            if(e) {
                e.preventDefault();
            }
            var login_data = {
                email : this.$get('email'),
                password : this.$get('password')
            };
            this.$http.post('/../login',login_data)
                .then(function (response) {
                    if(response && response.data && !response.data.message) {
                        this.$set('authentication',true);
                        this.$set('isAdministrator',response.data.isAdministrator);
                    } else if (response.data.message) {
                        this.$set('alertShow',true);
                        this.$set('alertMessage',response.data.message);

                    }
                    location.reload();
                },function (err) {
                    console.log(err);
                });

        },
        onLogout : function (e) {
            this.$http.get('/../login/logout')
                .then(function (response) {
                    if(response) {
                        // this.$set('authentication',false);
                        // this.$set('isAdministrator',false);
                        // this.$set('email','');
                        // this.$set('password','');
                        location.reload();
                    }
                },function (err) {
                    console.log(err);
                });
        },
        authenticate : function () {
            return this.$http.post('/../login/auth')
                .then(function (response) {
                    if(response && response.data && response.data.authentication) {
                        return response.data;
                    }
                    return false;
                },function (err) {
                    console.log(err);
                });
        }
    }
});
