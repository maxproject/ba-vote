/**
 * Created by The Exiled on 1/8/2559.
 */
var administrators = new Vue({
    el : '#administrators',
    components : {
        'modal': modal,
        'alert' : alert
    },
    ready : function () {
        var self = this;
        document.title = "จัดการข้อมูลผู้ใช้ผู้ดูแลระบบ";
        self.fetchAdministrators();
        app.authenticate()
            .then(function (response) {
                if(response && response.email) {
                    self.email = response.email;
                }
            },function (err) {
                console.log(err);
            });
    },
    data : {
        administrators : [],
        confirmRemoveAdmin : false,
        removeEmail : undefined,
        showAlert : false,
        alertMessage : 'คุณยังไม่ได้ทำรายการ',
        password : '',
        email : '',
        confirmPassword : '',
        isEditPassword : false,
        modalTitle : '',
        formModal : false,
        newEmail : undefined
    },
    methods : {
        fetchAdministrators: function () {
            this.$http.get('admin/list')
                .then(function (response) {
                    if (response && response.data) {
                        this.$set('administrators', response.data);
                    }
                }, function () {

                });
        },
        deleteAdministratorsByEmail: function (email) {
            this.$http.post('/admin/del', {email: email})
                .then(function (response) {
                    if (response && response.data) {
                        this.$set('showAlert', true);
                        this.$set('confirmRemoveAdmin', false);
                        this.$set('alertMessage', response.data.message);
                        this.fetchAdministrators();
                    }
                }, function (err) {
                    console.log(err)
                });
        },
        confirmModal: function (e) {
            e.preventDefault();
            var api = (this.isEditPassword) ? 'edit': 'add';
            var email = (this.isEditPassword) ? this.email: this.newEmail;
            var data = {
                email: email,
                passwords: this.$get('password')
            };
            this.$http.post('/admin/'+api, data)
                .then(function (response) {
                    if (response && response.data.message) {
                        this.$set('showAlert', true);
                        this.$set('formModal', false);
                        this.$set('alertMessage', response.data.message);
                        this.fetchAdministrators();
                    }
                }, function (err) {
                    console.log(err);
                });
        },
        showModal: function (bool) {
            this.$set('isEditPassword', bool);
            this.$set('modalTitle', (bool) ? 'แก้ไขรหัสผ่าน' : 'เพิ่มผู้ดูแลระบบ');
            this.$set('formModal', true);
        }
    }

});