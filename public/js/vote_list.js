/**
 * Created by The Exiled on 2/8/2559.
 */
var voteList = new Vue({
    el : '#voteList',
    ready : function () {
        var self = this;
        self.fetchBoys();
        self.fetchGirls();
        self.fetchLadyBoy();
        self.fetchTomBoy();

        app.authenticate()
            .then(function (response) {
                if (response && response.authentication) {
                    self.isLogin = response.authentication;
                    self.verifyStudentId(response.email);
                }

            },function (err) {
                console.log(err);
            });


        
    },
    data : {
        boys : [],
        girls : [],
        ladyBoys : [],
        tomBoys : [],
        isLogin : false,
        boyVoted : false,
        girlVoted : false,
        ladyBoyVoted : false,
        tomBoyVoted : false,
        studentId : undefined
    },
    methods : {
        fetchBoys : function () {
            var self = this;
            self.$http.get('/stars/all/B')
                .then(function (response) {
                    if (response && response.data) {
                        self.$set('boys',response.data);
                    }
                },function (err) {
                    console.log(err);
                });
        },
        fetchGirls : function () {
            var self = this;
            self.$http.get('/stars/all/G')
                .then(function (response) {
                    if (response && response.data) {
                        self.$set('girls',response.data);
                    }
                },function (err) {
                    console.log(err);
                });
        },
        fetchLadyBoy : function () {
            var self = this;
            self.$http.get('/stars/all/K')
                .then(function (response) {
                    if (response && response.data) {
                        self.$set('ladyBoys',response.data);
                    }
                },function (err) {
                    console.log(err);
                });
        },
        fetchTomBoy : function () {
            var self = this;
            self.$http.get('/stars/all/T')
                .then(function (response) {
                    if (response && response.data) {
                        self.$set('tomBoys',response.data);
                    }
                },function (err) {
                    console.log(err);
                });
        },
        isVoted : function (id) {
            var self = this;
            self.$http.get('/user/isVote/'+id)
                .then(function (response) {
                    if (response && response.data) {
                        var arr = response.data;
                        for (var i = 0 ; i < arr.length ; i++) {
                            var id = arr[i].star_id;
                            if (id.substring(0,1) === 'B') {
                                self.boyVoted  = true;
                            } else if (id.substring(0,1) === 'G') {
                                self.girlVoted  = true;
                            } else if (id.substring(0,1) === 'K') {
                                self.ladyBoyVoted  = true;
                            } else if (id.substring(0,1) === 'T') {
                                self.tomBoyVoted  = true;
                            }
                        }
                    }
                },function (err) {
                    console.log(err);
                });
        },
        verifyStudentId  : function (email) {
            this.$http.post('/register/edit',{email : email})
                .then(function (response) {
                    if(response && response.data) {
                        var data = response.data[0];
                        this.isVoted(data.student_id);
                        this.studentId = data.student_id;
                    }
                },function (err) {
                    console.log(err);
                });
        },
        voteStar : function (id) {
            this.$http.post('/stars/vote',{student_id : this.studentId , star_id : id})
                .then(function (response) {
                    if (response) {
                        location.reload();
                    }
                },function (err) {
                    console.log(err)
                });
        }
    }
});