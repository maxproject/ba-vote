/**
 * Created by The Exiled on 1/8/2559.
 */
var register = new Vue({
    el : '#form',
    ready : function () {
        var self = this;
        self.$http.get('user/list/major')
            .then(function (response) {
                if(response && response.data) {
                    this.$set('listMajor',response.data);
                }
            },function (err) {
                console.log(err);
            });


        app.authenticate()
            .then(function (response) {
                if(response) {
                    self.fetchUserProfile(response.email);
                    document.title = 'แก้ไขข้อมูลส่วนตัว';
                }else {
                    document.title = 'ลงทะเบียน';
                }
            },function (err) {
                console.log(err)
            });
    },
    data : {
        userEmail : '',
        userPassword : '',
        confirmPassword : '',
        studentId : '',
        studentName : '',
        birthdate : '',
        gender : '',
        userMajor : '',
        listMajor : [],
        isUpdate : false
    },
    methods : {
        onSubmit: function (e) {
            e.preventDefault();
            var self = this;
            var form = this.convertToRegister();
            var isUpdate = this.$get('isUpdate');
            var api = (!isUpdate)?'/register':'/register/update';
            this.$http.post(api,form)
                .then(function (response) {
                    console.log(response)
                    if(response.data.result && !isUpdate) {
                        var email = self.$get('userEmail');
                        var password = self.$get('userPassword');
                        app.$set('alertSuccessMsg', 'ลงทะเบียนเรียบร้อยแล้ว');
                        app.$set('alertSuccessShow', true);
                        app.$set('email', email);
                        app.$set('password', password);
                        // app.onLogin(); /** ถ้าไม่อยากให้ล๊อกอินให้ดีบักไว้ ***********************************************/
                        window.location = "http://localhost:3000/";



                    } else if(response.data.result && isUpdate) {
                        app.$set('alertSuccessMsg', 'แก้ไขข้อมูลเรียบร้อยแล้ว');
                        app.$set('alertSuccessShow', true);
                    } else {
                        var msg = (isUpdate)? 'คุณไม่สามารถแก้ไขข้อมูลส่วนตัวได้':'คุณไม่สามารถลงทะเบียนได้';
                        app.$set('alertMessage',msg);
                        app.$set('alertShow',true);
                    }

                },function (err) {
                    console.log(err)
                });
        },
        convertToRegister : function () {
            return {
                login_email : this.$get('userEmail'),
                passwords : this.$get('userPassword'),
                student_id : this.$get('studentId'),
                mem_birthday : this.$get('birthdate'),
                mem_name : (this.$get('studentName')).split(' ')[0],
                mem_lname : (this.$get('studentName')).split(' ')[1],
                mem_gender : this.$get('gender'),
                major_id : this.$get('userMajor')
            };
        },
        fetchUserProfile : function (email) {
            this.$http.post('/register/edit',{email : email})
                .then(function (response) {
                    if(response && response.data) {
                        var data = response.data[0];
                        this.$set('userEmail',data.email);
                        this.$set('userPassword',data.passwords);
                        this.$set('studentId',data.student_id);
                        this.$set('birthdate',data.mem_birthday);
                        this.$set('studentName',data.mem_name+' '+data.mem_lname);
                        this.$set('gender',data.mem_gender);
                        this.$set('userMajor',data.major_id);
                        this.$set('isUpdate',true);
                    }
                },function (err) {
                    console.log(err);
                });
        }
    }

});
