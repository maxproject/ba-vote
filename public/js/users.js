/**
 * Created by The Exiled on 1/8/2559.
 */
var users = new Vue({
    el : '#users',
    components : {
        'modal': modal,
        'alert' : alert
    },
    ready : function () {
        document.title = "จัดการข้อมูลผู้ใช้ผู้ใช้ทั่วไป";
        this.fetchUsers();
    },
    data : {
        users : [],
        confirmRemoveUser : false,
        removeEmail : undefined,
        showAlert : false,
        alertMessage : 'คุณยังไม่ได้ทำรายการ'
    },
    methods : {
        fetchUsers : function () {
            this.$http.get('user/list')
                .then(function (response) {
                    if(response && response.data) {
                        this.$set('users',response.data);
                        console.log(response.data)
                    }
                },function (err) {
                    console.log(err)
                });
        },
        deleteUserByEmail : function (email) {
            this.$http.post('/user/del',{email:email})
                .then(function (response) {
                    if(response && response.data) {
                        this.$set('showAlert',true);
                        this.$set('confirmRemoveUser',false);
                        this.$set('alertMessage',response.data.message);
                        this.fetchUsers();
                    }
                },function (err) {
                    console.log(err)
                });

        }
    }
});