/******************* Add Admin *******************************/

var connection = require("../../config/database");
var admin = {};

admin.newAdmin = function (req,res,next) {
  res.send("AddNewAdmin");
};

/***
 *ขั้นตอนการทำงานคือ รับค่าจากผู้ใช้
 * ต่อ Database
 * ติดต่อข้อมูลในตาราง
 * ทำการรับข้อมูลจากผู้ใช้
 * ทำการ แอดข้อมูล ลงใน database
 */
admin.addnewAdmin = function (req,res,next) {
  var email = req.body.email; //req.query.login_email; /------------------------ ต้องใช้บ่อย
  var passwords = req.body.passwords; //req.query.passwords;
  var sql_login = "INSERT INTO `mydb`.`tb_login` SET ?";
  var admin_data = {
    email : email,
    passwords : passwords,
    status : 0

  };
  connection.query(sql_login, admin_data, function (err, result) {
    if (!err) {
      res.send({ message: 'เพิ่มผู้ดูแลระบบเรียบร้อยแล้ว' });
    } else {
      res.send({ message: 'เกิดข้อผิดพลาดจากระบบ' });
    }
  });
  };
  /*********** List Admin ****************   */

  admin.adminList = function (req,res,next) {
        var sql_user = "select*from tb_login where status = 0;";

          connection.query(sql_user,function (err,rows,field) {
            if(!err){
              if(rows && rows.length >0){
                 res.json(rows);
              }else{
                res.send ({ message : "ไม่มีชื่อ Admin ในรายการนี้" });
              }
            }else {
                res.send ({ message : "ผิดพลาดในการค้นหาข้อมูล"});
        }
    })
  };

/************* Delete Admin ****************   */

  admin.adminDel = function (req,res,next) {

    var email = req.body.email;
    console.log(email);
    var sql_del = 'DELETE FROM tb_login WHERE email = \''+email+'\'';

    connection.query(sql_del ,function (err,result) {
        if(!err){
          res.send({ message : 'ทำการลบข้อมูลผู้ดูแลระบบเรียบร้อยแล้ว'  });
        }

        else {
          res.send({ message : 'เกิดเหตุขัดข้องไม่สามารถลบผู้ดูแลระบบได้' });
        }
    })
  };

admin.editPassword = function (request,response,next) {
  var edit_data = {
    passwords : request.body.passwords
};
  var sql = 'UPDATE mydb.tb_login SET ? WHERE email = \''+request.body.email+'\'';
  var responseResult = { message : 'ไม่สามารถแก้ไขรหัสผ่านได้ กรุณาลองใหม่อีกครั้ง' };
  var r = connection.query(sql , edit_data ,function (err , result) {
    if(!err) {
      responseResult.message = 'แก้ไขรหัสผ่านเรียบร้อยแล้ว';
    }
    response.send(responseResult);
  });
    console.log(r)
};
admin.adminGet = function (req,res,next) {

  var email = req.query.email;
  var sql_get = "select * from tb_login where email = ? ;";
  var callback = function (err,rows,field) {
    if(!err){
      if(rows && rows.length ===1){
        res.json(rows);
      }else{
        res.send ({ message : true });
      }
    }else{
      res.send({ message : false });
    }
  };
  connection.query(sql_get,[email] ,callback);

};

/*************   ****************   */

module.exports = admin;
//ฟังก์ชั่นที่ใช้งาน ใน register controller
