/**
 * Created by JJOMJAMM on 16/7/2559.
 */
var connection = require("../../config/database");
var register = {};

register.insertRegister = function (req,res,next) { //ส่วนของการรับค่าจาก"ผู้ใช้"
  var student_id = req.body.student_id; //req.body.student_id; เพราะเราใช้งานใน POSTMAN เราเลยต้องใช้ query แทนแต่ถ้าใช้งานจริงใช้ body
  var mem_name = req.body.mem_name; //req.body.mem_name;
  var mem_lname = req.body.mem_lname; //req.body.mem_lname;
  var login_email = req.body.login_email; //req.body.login_email;
  var mem_gender = req.body.mem_gender; //req.body.mem_gender;
  var passwords = req.body.passwords; //req.body.passwords;
  var mem_birthday = req.body.mem_birthday; //req.body.mem_birthday;
  var major_id = req.body.major_id;//req.body.mem_birthday;
  /**
   * *****************************************************
   * นี่คือตัวแปร ในการบันทึกข้อมูล
   * ตัวแปร sql_login คือคำสั่งเพิ่มข้อมูลลงใน ตาราง tb_login โดย เครื่องหมาย ? คือข้อมูลที่เราต้องการเพิ่มลงไป 
   * ตัวแปร sql_register คือคำสั่งเพิ่มข้อมูลลงในตาราง tb_member โดย เครื่องหมาย ? คือข้อมูลที่เราต้องการพิ่ม
     */
  var sql_login = "INSERT INTO `mydb`.`tb_login` SET ?";
  var sql_register = "INSERT INTO `mydb`.`tb_member` SET ?";
  
  /**
   * *****************************************************
   * ตัวแปร login_data คือตัวแปรที่เอาไว้ เก็บข้อมูล สำหรับ การบันทึกข้อมูลในตาราง tb_login
   * โดย ชื่อ Property(email,passwords) จะกำหนดให้ตรงกับชื่อของ คอลั่มในตาราง tb_login และค่าที่บันทึกจะเป็นข้อมูลสำหรับการบันทึกลงในแถวนั้นๆ
   * ค่าของ property จะรับมาจากผู้ใช้
   */
  var login_data = {
    email : login_email,
    passwords : passwords,
    status : 1
  };
  /**
   * *****************************************************
   * ตัวแปร sql_data คือตัวแปรที่เอาไว้ เก็บข้อมูล สำหรับ การบันทึกข้อมูลในตาราง tb_member
   * โดย ชื่อ Property(student_id,mem_name) จะกำหนดให้ตรงกับชื่อของ คอลั่มในตาราง tb_member และค่าที่บันทึกจะเป็นข้อมูลสำหรับการบันทึกลงในแถวนั้นๆ
   * ค่าของ property จะรับมาจากผู้ใช้
   */
  var sql_data = {
    student_id : student_id,
    mem_name : mem_name,
    mem_lname : mem_lname,
    login_email : login_email,
    mem_gender : mem_gender,
    mem_birthday :mem_birthday,
    major_id : major_id
  };
  //----------------------------------ส่วนจัดการ Login ใน register
  // console.log(sql_data)
  // res.send ("loginSucsess"); //คำสั่งทดสอบว่าผ่านไหม
  //---------------------------------ส่วนของ Register ใช้จัดการนข้อมูลลงใน ตาราง login,register
  console.log(sql_data);
  /**
   ****************************************************************************
   * connection.connect(); เป็นคำสั่งสำหรับการเชื่อมต่อ Database
   * connection.query(คำสั่ง sql , ข้อมูลสำหรับใช้กับคำสั่ง , function ที่เรียกใช้)
   * คำสั่ง sql คือ คำสั่งสำหรับการประมวลผลในฐานข้อมูลซึ่งคำสั่งนี้เป็นคำสั่งสำหรับการบันทึกข้อมูลลงในตาราง tb_login
   * "ข้อมูลสำหรับใช้กับคำสั่ง login_data" คือชุดข้อมูลสำหรับการบันทึก ลงในตาราง tb_login ซึ่งข้อมูลสำหรับการบันทึก (insert)จะเก็บเป็นตัวแปรประเภท Object
   * "function" เรียกใช้คือ การทำงานหลังจากได้ผลลัพธ์ จากคำสั่ง(sql_login,login_data) ซึ่งคำสั่งสำหรับการ insert จะคืนค่า error และผลลัพธ์การ insert
   */
  connection.query(sql_login,login_data,function (err, result){
    /*ตรวจสอบว่าผลลัพธ์ การบันทึกมี error หรือไม่*/
    if (!err){
      /**
       * เนื่องจากไม่มี error จะให้ทำการบันทึกฐานข้อมูล ลงในตาราง tb_member จะใช้
       * connection.query(คำสั่ง sql , ข้อมูลสำหรับใช้กับคำสั่ง , function ที่เรียกใช้)
       * --คำสั่ง sql คือ คำสั่งสำหรับการประมวลผลในฐานข้อมูลซึ่งคำสั่งนี้เป็นคำสั่งสำหรับการบันทึกข้อมูลลงในตาราง tb_member
       * "ข้อมูลสำหรับใช้กับคำสั่ง" คือชุดข้อมูลสำหรับการบันทึก ลงในตาราง tb_member ซึ่งข้อมูลสำหรับการบันทึก (insert)จะเก็บเป็นตัวแปรประเภท Object
       * "function" เรียกใช้คือ การทำงานหลังจากได้ผลลัพธ์ จากคำสั่ง(sql_register,sql_data) ซึ่งคำสั่งสำหรับการ insert จะคืนค่า error และผลลัพธ์การ insert
       */
      connection.query(sql_register,sql_data,function (err, result) {
        /*จะทำการยกเลิก การติดต่อ Database ทกครั้ง*/
       if(!err){
         /*เมื่อได้ผลลัพธ์ที่ถูกต้องจะนำข้อมูลผลลัพธ์กลับคืนให้ผู้ใช้*/
         res.send ({ result : true });
       }else {
         /*เมื่อการค้นหาผิดพลาดจะแจ้งเตือนผู้ใช้ว่าข้อมูลผิดพลาดคืนให้ผู้ใช้*/
         res.send(err);
       }
      });
    }else {
    }
    });
     //--------------------------------------------------


};

register.GetRegister = function (req,res,next) { //เอาไว้ดูตาราง email ของทั้งสอง ว่าตรงกันหรือไม่ และส่ง Json ออกมาเป็นข้อมูลดิบ
  var email = req.body.email; //req.body.email;
  /**
   * ตัวแปร sql_edit_register คือคำสั่งเรียกข้อมูลทั้งหมดทุกคอลั่ม จากตาราง tb_login และ tb_member โดยระบุจาก email ใช้ where
   * ตัวแปร data = [email,email]; คือ การเก็บตัวแปรที่เราต้องการไว้ใน array ซึ่งจะเรียงข้อมูลตาม ? ของคำสั่ง sql
     */
  var sql_edit_register = "select * from mydb.tb_login login join tb_member member where login.email = ? and member.login_email = ?" ;
  var data = [email,email];
  /**
   * connection.query(คำสั่ง sql , ข้อมูลสำหรับใช้กับคำสั่ง , function ที่เรียกใช้)
   * คำสั่ง sql คือ คำสั่งสำหรับการประมวลผลในฐานข้อมูลซึ่งคำสั่งนี้เป็นคำสั่งสำหรับการบันทึกข้อมูลลงในตาราง tb_login,tb_member
   * "ข้อมูลสำหรับใช้กับคำสั่ง" คือชุดข้อมูลสำหรับการบันทึก ลงในตาราง tb_member ซึ่งข้อมูลสำหรับการบันทึก (insert)จะเก็บเป็นตัวแปรประเภท Object
   * "function" เรียกใช้คือ การทำงานหลังจากได้ผลลัพธ์ จากคำสั่ง(sql_register,sql_data) ซึ่งคำสั่งสำหรับการ select จะคืนค่า error >
   * และผลลัพธ์แถวที่ทำการ query มาได้
   * 
   */
  connection.query(sql_edit_register,data,function (err,rows,field) {
    if(!err){
      /*ตรวจสอบผลลัพธ์ที่ได้ว่าไม่เป็นค่าว่าง และต้องได้แค่ 1 แถวเท่านั้น*/
      if(rows && rows.length ===1 ){ //แสดงข้อมูลออกมา 1 แถวโดยใช้ GET
      /* ส่งออกเป็นJson ให้กับผู้ใช้เป็นข้อมูลดิบ*/
        res.json(rows);
      }else {
        res.send({ getprofile : false });
      }


    }else {
      res.send({ getprofile : true });
    }

  })

};


register.UpdateRegister = function (req,res,next) { //ส่วนของการอัพเดทข้อมูลผู้ใช้
  var student_id = req.body.student_id; //req.body.student_id;
  var mem_name = req.body.mem_name; //req.body.mem_name;
  var mem_lname = req.body.mem_lname; //req.body.mem_lname;
  var login_email = req.body.login_email; //req.body.login_email;
  var mem_gender = req.body.mem_gender; //req.body.mem_gender;
  var passwords = req.body.passwords; //req.body.passwords;
  var mem_birthday = req.body.mem_birthday; //req.body.mem_birthday;
  var major_id = req.body.major_id;//req.body.mem_birthday;
  var sql_data = [mem_name, mem_lname, mem_gender, mem_birthday, major_id];
  //----------------------------------ส่วนจัดการ Login ใน register
  // console.log(sql_data)

    var sql_register = "UPDATE  tb_member SET mem_name = ? , mem_lname = ? , mem_gender = ? ,mem_birthday = ? ,major_id = ? where student_id = '"+student_id+"'";
    var sql_login = "UPDATE  tb_login SET passwords = ? where email = '"+login_email+"'";
  console.log(sql_login+'\n'+sql_register);
  //---------------------------------ส่วนของ Register ใช้จัดการนข้อมูลลงใน ตาราง login,register
  connection.query(sql_login,[passwords],function (err, result){

    if (!err){
      connection.query(sql_register,sql_data,function (err, result) {
        if(!err){
          res.send ({ result : true });
        }else {
          res.send(err);
        }
      });
    }else {
      res.send(err);
    }
  });
  //--------------------------------------------------


};
module.exports = register;
//ฟังก์ชั่นที่ใช้งาน ใน register controller


