var Stars = require('../models/stars.model');
var connection = require("../../config/database");
var multiparty = require("multiparty");
var fs = require('fs');
var controller = {};
controller.addStar = function (request,response,next) {
    var form = new multiparty.Form;
    form.parse(request,function (err,fields,files) {
        console.log(fields);
        var starProfile = {
            star_id : fields.starCode[0]+fields.starId[0],
            year : (new Date().getFullYear())+543,
            // year : 2560, //ใช้ทดสอบฟิ๊กปีนั้นๆ
            star_history : '-',
            star_name : (fields.starName[0]).split(' ')[0],
            star_lname : (fields.starName[0]).split(' ')[1],
            major_id : parseInt(fields.major[0])
        };
        var sql = "insert into tb_star set ?";
        var result =connection.query(sql,starProfile,function (err,result) {
           if(!err) {
               var image = files.profile_pic[0];
               fs.readFile(image.path,function (error,data) {
                  if(!error) {
                      var store = './public/img/'+starProfile.star_id+'_'+starProfile.year+'.jpg';
                      fs.writeFile(store,data,function (uploadErr) {
                          if(uploadErr) {
                              response.send(uploadErr)
                          }else {
                              response.redirect('/../stars');
                          }
                      })
                  } else {
                      response.send(error);
                  }
               });
           } else {
               response.send(err);
           }
        });
        console.log(result)
    });
};

controller.allStudent = function (req,res,next) {
    var currentYear = ((new Date().getFullYear()+543)+"").substring(2,4);
    // var currentYear = 2560;
    var yearId = parseInt(currentYear);
    var years = [yearId-3,yearId-2,yearId-1,yearId,yearId-3,yearId-2,yearId-1,yearId];
    var sql_user = "select SumAllStd(?,?,?,?) as TotalStudent ,SumStarVote (?,?,?,?) as TotalScoreVote;";
    connection.query(sql_user,years,function (err,rows,field){

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีรายการผู้ใช้"});

            }

        }else{
            console.log(err);
            res.send(err);
        }
    });

};

/*************** ส่วนของรายการโหวต ************************/
controller.strVoteTypeBoy = function (req,res,next) {
    var year = req.params.year;
    var sql_user = "select star_id , year ,star_name ,star_lname ,major_name " +
        "from tb_star ts join tb_major tm where ts.major_id = tm.major_id and star_id like 'B%' and year = ?" +
        " group by star_id order by length(star_id),star_id asc;";


    connection.query(sql_user,[year],function (err,rows,field) {

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีประเภทโหวต เดือน ในนี้"});

            }

        }else{
            res.send(err);
        }
    });

};

controller.strVoteTypeGirl = function (req,res,next) {
    var year = req.params.year;
    var sql_user = "select star_id , year ,star_name ,star_lname , major_name " +
        "from tb_star ts join tb_major tm where ts.major_id = tm.major_id and star_id like 'G%' and year = ?" +
        " group by star_id order by length(star_id),star_id asc;";

    connection.query(sql_user,[year],function (err,rows,field) {

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีประเภทโหวต ดาว ในนี้"});

            }

        }else{
            res.send(err);
        }
    });

};

controller.strVoteTypeLadyBoy = function (req,res,next) {
    var year = req.params.year;

    var sql_user = "select star_id , year ,star_name ,star_lname , major_name " +
        "from tb_star ts join tb_major tm where ts.major_id = tm.major_id and  star_id like 'L%' and year = ?" +
        " group by star_id order by length(star_id),star_id asc;";


    connection.query(sql_user,[year],function (err,rows,field) {

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีประเภทโหวต ดาวเทียม ในนี้"});

            }

        }else{
            res.send(err);
        }
    });

};

controller.strVoteTypeTomBoy = function (req,res,next) {
    var year = req.params.year;
    var sql_user = "select star_id , year ,star_name ,star_lname , major_name " +
        "from tb_star ts join tb_major tm where ts.major_id = tm.major_id and star_id like 'T%' and year = ?" +
        " group by star_id order by length(star_id),star_id asc;";


    connection.query(sql_user,[year],function (err,rows,field) {

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีประเภทโหวต เดือนเทียม ในนี้"});

            }

        }else{
            res.send({ message : "เกิดข้อผิดพลาด"});
        }
    });

};

/*************** จบส่วนของรายการโหวต ************************/


/*************** ส่วนของการโชว์คะแนน ************************/

controller.strScoreTypeBoy = function (req,res,next) {

    var sql_user = "select tv.star_id , count(tv.star_id) as vote_count , star_year,star_name ,star_lname  " +
        "from tb_vote tv join tb_star ts where tv.star_id = ts.star_id and " +
        "tv.star_year = ts.year and tv.star_id like 'B%' and star_year = ? " +
        "group by tv.star_id order by  count(tv.star_id) desc LIMIT 5;";


    connection.query(sql_user,[new Date().getFullYear()+543],function (err,rows,field) {

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีประเภทโหวต เดือน ในนี้"});

            }

        }else{
            res.send({ message : "เกิดข้อผิดพลาด"});
        }
    });

};

controller.strScoreTypeGirl = function (req,res,next) {
    var sql_user = "select tv.star_id , count(tv.star_id) as vote_count , star_year,star_name ,star_lname  " +
        "from tb_vote tv join tb_star ts where tv.star_id = ts.star_id and " +
        "tv.star_year = ts.year and tv.star_id like 'G%' and star_year = ? " +
        "group by tv.star_id order by  count(tv.star_id) desc LIMIT 5;";


    connection.query(sql_user,[new Date().getFullYear()+543],function (err,rows,field) {

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีประเภทโหวต ดาว ในนี้"});

            }

        }else{
            res.send(err);
        }
    });

};

controller.strScoreTypeLadyBoy = function (req,res,next) {
    var sql_user = "select tv.star_id , count(tv.star_id) as vote_count , star_year,star_name ,star_lname  " +
        "from tb_vote tv join tb_star ts where tv.star_id = ts.star_id and " +
        "tv.star_year = ts.year and tv.star_id like 'K%' and star_year = ? " +
        "group by tv.star_id order by  count(tv.star_id) desc LIMIT 5;";


    connection.query(sql_user,[new Date().getFullYear()+543],function (err,rows,field) {

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีประเภทโหวต ดาวเทียม ในนี้"});

            }

        }else{
            res.send({ message : "เกิดข้อผิดพลาด"});
        }
    });

};

controller.strScoreTypeTom = function (req,res,next) {
    var sql_user = "select tv.star_id , count(tv.star_id) as vote_count , star_year,star_name ,star_lname  " +
        "from tb_vote tv join tb_star ts where tv.star_id = ts.star_id and " +
        "tv.star_year = ts.year and tv.star_id like 'T%' and star_year = ? " +
        "group by tv.star_id order by  count(tv.star_id) desc LIMIT 5;";


    connection.query(sql_user,[new Date().getFullYear()+543],function (err,rows,field) {

        if (!err) {
            if(rows && rows.length > 0){
                res.json(rows);
            }else{
                res.send({ message : "ไม่มีประเภทโหวต เดือนเทียม ในนี้"});

            }

        }else{
            res.send({ message : "เกิดข้อผิดพลาด"});
        }
    });

};

/*************** จบส่วนของการโชว์คะแนน ************************/

/*************** ส่วนของการลบดาวเดือน ************************/

controller.removeStar = function (req,res,next) {
    var starId = req.body.starId;
    var year = req.body.year;

    var sql_vote = 'DELETE FROM tb_vote WHERE star_id = \''+starId+'\' and year = \''+year+'\'';
    var sql_star = 'DELETE FROM tb_star WHERE star_id = \''+starId+'\' and year = \''+year+'\'';
    connection.query(sql,function (err,result) {
        if (!err) {
            console.log(result)
            res.send({message : 'ลบข้อมูลออกจากระบบเรียบร้อยแล้ว'});
        } else {
            res.send ({ message : 'เกิดข้อผิดพลาดจากระบบไม่สามารถลบข้อมูลได้' });
        }
    });
};
/*************** จบส่วนของการลบดาวเดือน ************************/

/*************** ส่วนของการเรียกดูดาวเดือน และ แสดงชื่อสาขาแบบตัวอักษร ************************/
controller.fullStarNameMajor = function (req,res,next) {
    var sql_user = 'select tb_star.star_id , tb_star.year , tb_star.star_name , tb_star.star_lname , tb_major.major_name ' +
        'from tb_major inner join tb_star on (tb_major.major_id = tb_star.major_id) order by  (star_id);';

    connection.query(sql_user,function (err,rows,field) {
        if(!err){
            if(rows && rows.length >0){
                res.json(rows);
            }else{
                res.send ({ message : "ไม่มีชื่อ User นี้ในรายการ" });
            }
        }else {
            res.send (err);
        }
    })
};
/*************** จบส่วนของการเรียกดูดาวเดือน และ แสดงชื่อสาขาแบบตัวอักษร **********************/

/*************** ส่วนของ ************************/


controller.listStar = function (req,res,next) {
    var year = new Date().getFullYear() + 543;
    var type = req.params.type+'%';
    var sql = 'select * from tb_star s join tb_major m where s.tb_major_major_id = m.major_id and star_id like ? and year = ? order by length(star_id),star_id asc ';
    connection.query(sql,[type,year],function (err,row,field) {
        if (!err) {
            if (row && row.length > 0) {
                res.send(row);
            }else {
                res.send({
                    message : 'ไม่พบข้อมูล'
                })
            }
        }else {
            res.send(err);
        }
    });
};

controller.voteStar = function (req,res,next) {
    var student_id = req.body.student_id;
    var star_id = req.body.star_id;
    var data = {
        student_id : student_id,
        star_id : star_id,
        star_year : (new Date().getFullYear()+543)
    };

    var sql = 'insert into tb_vote set ? ';
    connection.query(sql,data,function (err ,result) {
       if (!err) {
           res.send({message : 'คุณทำการโหวตหมายเลข'+star_id+'เรียบร้อยแล้ว'});
       } else {
           res.send(err);
       }
    });
};



module.exports = controller;
