var express = require('express');
var router = express.Router();

/* GET home page. */
var login = require("../controllers/login.controller");

router.get('/', login.renderPage);
router.post('/', login.verifyLogin);
router.get('/logout', login.logout);
router.post('/auth', function (req,res,next) {
    var authenticate = false;
    var isAdministrator = false;
    var email = undefined;
    console.log(req.session.authentication);
    if(req.session.authentication === 'user' || req.session.authentication === 'administrator') {
        authenticate = true;
        isAdministrator = req.session.authentication === 'administrator';
        email = req.session.email;
    }
    res.send({
        authentication : authenticate,
        isAdministrator : isAdministrator,
        email : email
    });
});

module.exports = router;

