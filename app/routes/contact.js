var express = require('express');
var router = express.Router();
var contact = require('../controllers/contact.controller');

router.get('/', function (req,res,next) {
    res.render('contract');
});
router.post('/send', contact.sendEmail);


module.exports = router;
