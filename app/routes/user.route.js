/**
 * Created by JJOMJAMM on 18/7/2559.
 */

var express = require('express');
var router = express.Router();

var user = require("../controllers/user.controller");
router.get('/list' ,user.listUser);
router.post('/del' ,user.delUser);
router.get('/list/major' ,user.listMajor);


router.get('/list/user',user.listOneUser);
router.get('/isVote/:id',user.checkUserVote);

router.get('/list/boy',user.useVoteTypeBoy);
router.get('/list/girl',user.useVoteTypeGirl);
router.get('/list/ladyboy',user.useVoteTypeLadyBoy);
router.get('/list/tomboy',user.useVoteTypeTomBoy);




router.get('/',function (req,res,next) {
    if(req.session.authentication !== 'administrator' ) {
        res.redirect('/');
    }
    res.render('users');
});

module.exports = router;
