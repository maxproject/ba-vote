'use strict';
var gulp = require("gulp");
var browserSync = require('browser-sync');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var pump = require('pump');
var clean = require('gulp-clean');
var sass = require('gulp-sass');

var path = {
    sass : './public/sass/*.scss',
    css : './public/css/*.css',
    js : './public/js/*.js',
    ejs : './app/views/*.ejs',
    templates : './app/view/templates/*ejs',
    css_min : './public/css/*.min.css',

};

gulp.task('default', ['browser-sync','css-minify'], function () {
    gulp.watch(path.sass,['css-minify']);
    gulp.watch([path.ejs,path.css_min,path.js,path.templates], browserSync.reload);
});

gulp.task('browser-sync',  function() {
    browserSync.init(null, {
        port : 3001,
        proxy : 'http://localhost:3000'
    });
});

gulp.task('css-clean', function () {
    return gulp.src(path.css_min)
        .pipe(clean({force: true}));
});

gulp.task('sass', function () {
    return gulp.src(path.sass)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('css-minify',['css-clean','sass'], function() {
    return gulp.src(path.css)
        .pipe(concat('style.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/css'));
});


